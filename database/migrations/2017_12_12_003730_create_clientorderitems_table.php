<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientorderitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientorderitems', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('orderid');
            $table->integer('productid');

            $table->foreign('orderid')
                  ->references('id')->on('clientorders')
                  ->onDelete('cascade');
            $table->foreign('productid')
                  ->references('id')->on('products')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientorderitems');
    }
}
