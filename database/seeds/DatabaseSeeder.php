<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('roles')->insert([
            'id' => 'admin',
            'description' => 'administrator'
        ]);
        DB::table('roles')->insert([
            'id' => 'client',
            'description' => 'client user'
        ]);

    }
}
