@extends('layouts.app')
<link href="{{ asset('css/homepage.css') }}" rel="stylesheet">
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-3 col-md-2 col-lg-2">
            <div class="panel panel-default" style="border:">
              <div class="panel-heading">Categories</div>
              <div clas="panel-body">

                <ul class="list-group">

                  @foreach ($categories as $category )
                    <li class="list-group-item">{{ $category->name }}</li>
                  @endforeach

                </ul>

              </div>
            </div>
        </div>

        <div class="col-sm-9 col-md-6 col-lg-6">
            <div class="panel panel-default">
              <div class="panel-heading">Products</div>
              <div clas="panel-body">

                <ul class="list-group">

                  @foreach ($categories as $category)
                    <li class="list-group-item">------ {{ $category->name }}</li>

                    @if (!$category->products->first())
                      <li class="list-group-item">No products for this category</li>
                    @else
                      @foreach($category->products as $product)
                      <li class="list-group-item">
                        <div class="flex-container">
                          <div style="flex-grow: 20">{{ $product->name }}</div>
                            <div style="flex-grow: 1">€ 3.50</div>

                              <button style="flex-grow: 1" class="add-to-cart-button" id="{{$product->id}}" >Add</button>

                            </div>
                        <div class="">ingredienti...</div>
                      </li>
                      @endforeach

                    @endif
                  @endforeach
                </ul>

              </div>
            </div>
        </div>

        <!-- testing purpose cart saved on an array  --->
        <?php
      $vars['cartStarted'] = "true";
      $vars['cart'][0] = [1, 1];
      $vars['cart'][1] = [3, 2];
      $cart = $vars['cart'];
      ?>

        @if(isset($vars['cartStarted']))

        <div class="col-sm-12 col-md-4 col-lg-4">
            <div class="panel panel-default" style="border:">
              <div class="panel-heading">Cart</div>
              <div clas="panel-body">

                <ul class="list-group">
                  <?php
                  $cartProducts = $vars['cart'];
                  ?>
                  @foreach ($cartProducts as $prod)
                  <li class="list-group-item">{{$prod[1]}}x {{$products->get($prod[0])->name}}</li>
                  @endforeach
                </ul>

              </div>
            </div>
        </div>


      @endif


    </div>
</div>


  <script type="text/javascript">

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });


      $(document).ready(function(){
        $('.add-to-cart-button').click(function(e){
            e.preventDefault();


            $.ajax({
              type: 'POST',
              url:'/ajaxRequest',
              data: {product_id:this.id},
              dataType: "json",

              success:function(data){
                window.location.href = '/';
              }
            });

             console.log("pressed id "+ this.id);

        });
      });


  </script>

@endsection
