<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>{{ config('app.name', 'Restoonline') }}</title>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>

    <!-- Custom styles for this template -->
    <link href="/css/dashboard.css" rel="stylesheet">
  </head>

  <body>
    <header>
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">{{ config('app.name', 'Restoonline') }}</a>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Cart</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Account</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Help</a>
            </li>
          </ul>
          @guest
          <button class="btn btn-default" type="button" >
            <a href="{{ route('login') }}">Login</a><span class="caret"></span></button>

          <button class="btn btn-default" type="button">
            <a href="{{ route('register') }}">Register</a><span class="caret"></span></button>
          @else
              <li class="dropdown">
                <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                  {{ Auth::user()->name }}
                <span class="caret"></span></button>

                <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Cart</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Account</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Help</a></li>
                  <li role="presentation" class="divider"></li>
                  <li role="presentation"><a href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                      Logout
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                  </form>

                </li>
                </ul>


              </li>
          @endguest


        </div>
      </nav>
    </header>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
          <ul class="nav nav-pills flex-column">
            <li class="nav-item">
              <a class="nav-link active" href="#">Overview <span class="sr-only">(current)</span></a>
            </li>
            @foreach ($categories as $category )
              <li class="nav-item">
                <a class="nav-link" href="#">{{ $category->name }}</a>
              </li>
            @endforeach


          </ul>


        </nav>

        <main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">

          <div class="container">

            <table class="table">

              @foreach ($categories as $category)
                <thead>
                  <tr>
                    <th>{{ $category->name }}</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                @if (!$category->products->first())

                <tr>
                  <td>No products for this category</td>
                  <td></td>
                  <td></td>
                </tr>

                @else
                  @foreach($category->products as $product)
                <tr>
                  <td>{{ $product->name }}</td>
                  <td>€3.50</td>
                  <td><button style="flex-grow: 1" class="add-to-cart-button" id="{{$product->id}}" >Add</button></td>
                </tr>
                @endforeach

                @endif
                @endforeach


              </tbody>
            </table>
          </div>


        </main>
      </div>
    </div>
  </body>





</html>
