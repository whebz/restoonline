<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">

        <section class="col-sm-3 col-md-2 hidden-sm-down">
          <h6>Categorie</h6>
          <div class="panel panel-default">
            <div class="panel-body">elenco categorie prodotti</div>
          </div>
        </section>

        <main role="main" class="col-sm-6 ml-sm-auto col-md-8 pt-3">
          <h6>menu</h6>
          <div class="panel panel-default">
            <div class="panel-body">elenco prodotti</div>
          </div>
        </main>
        <section class="col-sm-3 col-md-2 hidden-xs-down">
          <h6>carello</h6>
          <div class="panel panel-default">
            <div class="panel-body">contenuto carello va qui</div>
          </div>
        </section>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>