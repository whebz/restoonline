<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    //    $this->middleware('auth'); // l'ho rimosso così anche se non sono loggato posso vedere la home con i prodotti, in un altro Controller
                                     // metterò che quando clicco ordina, fa la middleware auth...
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $categories = \App\Category::all();
      $products = \App\Product::all();
      $cart = \Session::get('cart');


        // layout was home
        return view('layout', compact('categories'), compact('products'), compact('cart'));
    }



    public function ajaxRequest() {
        return view('ajaxRequest');
    }

    public function ajaxRequestPost() {

        $input = request()->all();

        $prod = request()->input('product_id');



        return response()->json(['success'=>'Got Simple Ajax Request.']);
    }

}
